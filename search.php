<?php
    session_start();
    include('header.php');
    
?>

<div class = "container">
    <div class="col-lg-6">
        <div class="input-group">
            <input type="text" id = "searchTag" class="form-control" placeholder="Search for...">
            <span class="input-group-btn">
            <button id = "submit" class="btn btn-secondary" type="button">Go!</button>
            </span>
        </div>
    </div>
</div>
<br>
<div class = "container">
    <div id = "jobResults">
    </div>
</div>
